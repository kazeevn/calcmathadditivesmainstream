#include<stdio.h>
const int N = 1001;

int main(int argc, char** argv) {
  // TODO() arrays
  double epsilon_d[N];
  epsilon_d[1000] = 1;
  while ((1 + epsilon_d[1000]/2.0) != 1)
    for (int i = 0; i < N; ++i)
      epsilon_d[i] /= 2;

  float epsilon_f[N];
  epsilon_f[N - 1] = 1;
  for (int i = 0; i < N; ++i)
      epsilon_f[i] = 1;
  bool stop = false;
  while ( ! stop) {
    for (int i = 0; i < N; ++i)
      if ((1 + epsilon_f[N - 1]/2.0) == 1)
        stop = true;
    for (int i = 0; i < N; ++i)
      epsilon_f[i] /= 2;
  }
  printf("Float: %.20g, double: %.20g\n", epsilon_f[1000], epsilon_d[1000]);
}

