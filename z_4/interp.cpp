#include <iostream>
#include <fstream>
#include <math.h>
#include <eigen3/Eigen/Dense>
#define _USE_MATH_DEFINES
const int N = 6;
// We chose to have +1, so the N would correspond to n from the task

int main() {
  /* https://en.wikipedia.org/wiki/Polynomial_interpolation
     for explanation */
  Eigen::Matrix<double, N+1, N+1> X;
  // Vandermonde matrix
  Eigen::Matrix<double, N+1, 1> y;
  // Known values at the points
  Eigen::Matrix<double, N+1, 1> a;
  // Unknown polynomial coefficients

  // Filling the Vandermonde matrix and values vecor
  for (int k = 0; k <= N; ++k) {
    double x = k*M_PI/(4.0*N);
    y(k) = sin(x);
    for (int n = 0; n <= N; ++n)
      X(k,n) = pow(x, n);
    // Here we depend on pow(0,0) = 1
  }

  // Filling the values vector
  a = X.colPivHouseholderQr().solve(y);
  std::ofstream outfile;
  outfile.open("coeffs.dat");
  outfile.precision(15);
  outfile << a << std::endl;
  outfile.close();
  return 0;
}
