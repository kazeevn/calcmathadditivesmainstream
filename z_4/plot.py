#!/usr/bin/python
import numpy as np
import pylab as pl

coeffs = np.genfromtxt("coeffs.dat")
def get_val(x):
    return reduce(lambda t, y: t+coeffs[y]*x**y, range(len(coeffs)))

x = np.linspace(0,np.pi/4.0,num=300)
y_exp = map(get_val, x)
y_theor = np.sin(x)
fig, ax = pl.subplots()
ax.set_yscale('log')
ax.plot(x,np.abs(y_exp-y_theor))
pl.xlabel(r"$x$")
pl.ylabel(r"$|P_%d(x) - \sin(x)|$" % (len(coeffs)-1))
fig.savefig("interp.pdf")

