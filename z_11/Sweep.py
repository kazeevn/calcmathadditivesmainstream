import numpy as np
import pylab as pl
N = 50


x, step = np.linspace(0, np.pi, num=N+1, retstep=True)
A = -step**2/2.0/(1-np.cos(step))
y = A * np.sin(x)
p = np.ones(N)
q = np.ones(N)
p[0] = -0.5
q[0] = np.sin(0)*step*(-0.5)
for i in xrange(1,N):
    p[i] = 1/(-2.0-p[i-1])
    q[i] = (np.sin(x[i+1])*step**2-q[i-1])/(-2.0-p[i-1])

ys = np.ones(N)
ys[N-1] = q[N-1]
for i in xrange(N-2, -1, -1):
    ys[i] = q[i]-p[i]*ys[i+1]

fig, ax = pl.subplots()
y = y[0:-1]
x = x[0:-1]
pl.scatter(x, abs(ys-y), label="N=%d"%N)
#ax.set_yscale('log')
pl.legend()
