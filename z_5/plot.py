#!/usr/bin/python

import numpy as np
import pylab as pl

def gen_color():
    for color in ('r', 'g', 'b'):
        yield color

fig, ax = pl.subplots()
ax.set_yscale('log')
data = dict()
get_color = gen_color().next
for filename in ("rk.dat", "euler.dat"):
    y_exp = np.genfromtxt(filename)
    data[filename] = y_exp
    x = np.linspace(0, 3, num=len(y_exp))
    y_th = np.exp(-x)
    ax.scatter(x, abs(y_exp-y_th), label=filename, c=get_color())
pl.xlabel(r"$x$")
pl.ylabel(r"$|y - \exp(-x)|$")
pl.legend()
fig.savefig("odr.pdf")
