#include <eigen3/Eigen/Dense>
#include <iostream>
#include <fstream>

const int N = 15;
const double h = 3.0/N;
const double h2 = h*h;

int main() {
  Eigen::Matrix<double, N+1, 1> x;
  // Euler
  x(0) = 1;
  for (int i=0; i < N; ++i)
    x(i+1) = x(i) * (1-h);
  std::ofstream outfile;
  outfile.open("euler.dat");
  outfile << x << std::endl;
  outfile.close();
  // RK2
  // https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods
  x(0) = 1;
  for (int i=0; i < N; ++i)
    x(i+1) = x(i) * (1-h+0.5*h2);
  outfile.open("rk.dat");
  outfile << x << std::endl;
  outfile.close();
  return 0;
}
