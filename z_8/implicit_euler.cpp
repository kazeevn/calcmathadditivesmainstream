#include <iostream>
#include <fstream>
#include <eigen3/Eigen/Dense>
const int N = 10000;
const double T = 2;
const double h = T/N;

int main() {
  // https://en.wikipedia.org/wiki/Implicit_Euler_method
  Eigen::Matrix2d A;
  A << 998, 1998, -999, -1999;
  Eigen::Matrix2d B;
  Eigen::Matrix<double, 2, N+1> Sol;
  Sol.col(0) << -1, -1;
  B = Eigen::Matrix2d::Identity() - h*A;
  std::cout << B;
  for (int i = 1; i <= N; ++i)
    Sol.col(i) = B.colPivHouseholderQr().solve(Sol.col(i-1));
  std::ofstream outfile;
  outfile.open("euler.dat");
  outfile << Sol << std::endl;
  outfile.close();
  return 0;
}

