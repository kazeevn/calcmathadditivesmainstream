#!/usr/bin/python

import numpy as np
import pylab as pl

data = np.genfromtxt("euler.dat")
fig, ax = pl.subplots()
N = np.arange(len(data[0]))
ax.plot(N, data[0], label="$u$")
ax.plot(N, data[1], label="$v$")
ax.set_xlabel("N")
ax.set_ylabel("$u,v$")
pl.legend()
pl.grid()
fig.savefig("implicit_euler.pdf")
