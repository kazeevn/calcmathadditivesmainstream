// Copyright 2013, Nikita Kazeev
#include<stdio.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <eigen3/Eigen/Dense>
#define _USE_MATH_DEFINES
const int N = 400;
// We chose to have +1, so the N would correspond to the last index
const int t_iter = 400;
const double h = 15.0/N;
const double T = 2;
const double dt = T/t_iter;


int main() {
  Eigen::Matrix<double, N+1, t_iter+1> u;
  for (int i = 0; i <= N; ++i) {
    double x = i*h - 5;
    u(i, 0) = exp(-x*x/2);
  }
  for (int t = 1; t <= t_iter; ++t)
    for (int x = 1; x < N; ++x) {
      Eigen::MatrixXd p = u.col(t - 1);
      u(x,t) = p(x) - dt/(6*h)*(
          p(x)*(p(x + 1) - p(x - 1)) + p(x + 1)*
          p(x + 1) - p(x - 1)*p(x - 1));
    }
    std::ofstream outfile;
  outfile.open("hopf.dat");
  outfile << u;
  outfile.close();
  return 0;
}
