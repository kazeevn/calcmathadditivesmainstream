#!/usr/bin/python

import numpy as np
import pylab as pl

from mpl_toolkits.mplot3d import Axes3D
fig = pl.figure()
ax = fig.add_subplot(111, projection='3d')
data = np.genfromtxt("hopf.dat").T
# data[x][t]
Y,X=np.meshgrid(np.linspace(-5,10,num=len(data[0])),
                np.linspace(0,2,num=len(data)))
ax.plot_surface(X,Y,data)
ax.set_xlabel("x")
ax.set_xlabel("t")
ax.set_zlabel("u")
fig.savefig("hopf.pdf")

fig_max, ax_max = pl.subplots()
X = np.linspace(-5,10,num=len(data[0]))
t = np.linspace(0,2,num=len(data))
max_pos = map(lambda xs: X[xs.argmax()], data)
ax_max.plot(t, max_pos)
ax_max.set_xlabel("t")
ax_max.set_ylabel("Maximum position")
fig_max.savefig("hopf_max_position.pdf")
