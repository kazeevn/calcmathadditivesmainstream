#include <iostream>
#include <fstream>
#include <eigen3/Eigen/Dense>
#include <stdlib.h>

const int N = 100000;
const double T = 100;
const double h = T/N;
const int a = 10;
const int b = 2;
const int c = 2;
const int d = 10;

Eigen::Vector2d k(const Eigen::Vector2d X) {
  Eigen::Vector2d res;
  double x = X(0);
  double y = X(1);
  res(0) = a*x - b*x*y;
  res(1) = c*x*y - d*y;
  return res;
}

int main(int argc, char** argv) {
  Eigen::Matrix<double, 2, N+1> Sol;
  if (argc == 3)
    Sol.col(0) << atof(argv[1]), atof(argv[2]);
  else
    Sol.col(0) << 1, 1;
  for (int i = 0; i < N; ++i)
    Sol.col(i+1) = Sol.col(i) + h*k(Sol.col(i) + 0.5*h*k(Sol.col(i)));
  std::ofstream outfile;
  char outname[200];
  sprintf(outname, "hunter_prey_%s_%s.dat", argv[1], argv[2]);
  outfile.open(outname);
  outfile << Sol << std::endl;
  outfile.close();
  return 0;
}
