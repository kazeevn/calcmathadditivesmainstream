#!/usr/bin/python

import numpy as np
import pylab as pl
import sys

fig, ax = pl.subplots()

for filename in sys.argv[1:]:
    data = np.genfromtxt(filename)
    ax.plot(data[0], data[1], label=filename)

ax.set_xlabel("$x$")
ax.set_ylabel("$y$")
pl.legend()
fig.savefig("hunter_prey.pdf")
