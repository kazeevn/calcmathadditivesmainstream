// Copyright 2013, Nikita Kazeev
#include<stdio.h>
inline int minus_pow(const int n) {
    return (n % 2 == 0) ? 1 : -1;
}

int main() {
  float sum = 0;
  double answer = -0.6930971830599452969172324;
  // The 20-digits correct answer by Mathematica
  for (int n = 1; n <= 10e3; ++n)
    sum += minus_pow(n)/static_cast<double>(n);
  printf("Together up: %.20g\n", sum-answer);
  sum = 0;
  for (int n = 10e3; n > 0; --n)
    sum += minus_pow(n)/static_cast<float>(n);
  printf("Together down: %.20g\n", sum-answer);
  /* The most accurate answer. Why? The relative error on each
     addition/subtraction operation is minimal when both numbers are
     of the same order. By counting the small terms first, we keep
     both the current sum and the added term roughly equal.
  */
  float sum_n = 0;  // Holds positive sum
  float sum_p = 0;  // Holds negative sum
  for (int n = 1; n <= 10e3; ++n)
    if (n % 2 == 0)
      sum_p += 1/static_cast<float>(n);
    else
      sum_n += 1/static_cast<float>(n);
  printf("Separate up: %.20g\n", sum_p - sum_n - answer);
  sum_n = 0;
  sum_p = 0;
  for (int n = 10e3; n > 0; --n)
    if (n % 2 == 0)
      sum_p += 1/static_cast<float>(n);
    else
      sum_n += 1/static_cast<float>(n);
  printf("Separate down: %.20g\n", sum_p - sum_n - answer);
}
