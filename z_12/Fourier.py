import numpy as np
import pylab as pl

def Hann(t,T):
    return (1-np.cos(2*np.pi*t/T))/2.0

def Fourier(f, T, N):
    t = np.linspace(0,T,num=2*N)
    ms = np.linspace(0,2*N-1,2*N)
    c = np.ones(2*N)
    for m in ms:
        wm = 2*np.pi/T*m
        c[m] = np.abs(np.sum(f(t)*np.exp(1j*wm*t)))
    return c

N = 1e3
T = 2*np.pi
w = 500.5
data = Fourier(lambda x: np.sin(x*w), 2*np.pi, N)
data_Hann = Fourier(lambda x: Hann(x,T)*np.sin(x*w), T, N)
ws = np.linspace(0,2*N-1,2*N)
fig, ax = pl.subplots()
ax.set_yscale('log')
ax.plot(ws,data_Hann, lw = 3,label="Hann")
ax.plot(ws,data,'r')

pl.legend()
fig.show()
