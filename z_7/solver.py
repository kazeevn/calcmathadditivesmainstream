import numpy as np
import pylab as pl

def markers():
    while True:
        for i in (".", "o", "x", "*"):
            yield i

def bisection(f, df, interval, N):
    # f must invrease !
    t = np.mean((interval[1],interval[0]))
    if N == 0:
        return [t]
    else:
        F = f(t)
        if F > 0:
            return [t]+(bisection(f,df, (interval[0], t), N - 1))
        elif F < 0:
            return [t]+(bisection(f,df, (t, interval[1]), N - 1))
        else:
            return [t]
def newton(f, df, x0, N):
    if hasattr(x0, '__iter__'):
        x0 = np.mean(np.array(x0))
    if N == 0:
        return [x0]
    else:
        res = x0 - f(x0)/df(x0)
        return [res]+newton(f,df,res,N-1)
def iterations(phi,x0,N):
    # f must be in form x+phi(x)==0
    if hasattr(x0, '__iter__'):
        x0 = np.mean(np.array(x0))
    if N==0:
        return [x0]
    else:
        res = phi(x0)
        return [res]+iterations(phi,res,N-1)

methods = tuple([(bisection, "Bisection"),(newton, "Newton"),(iterations,"Simple Iterations")])
Num = 30

def run(f, intervals, phi=None):
    fig, ax = pl.subplots()
    get_marker = markers()
    for interval in intervals:
        for method in methods:
            if method[0]==iterations:
                res = np.array(zip(method[0](phi, interval, Num), range(Num))).T
            else:
                res = np.array(zip(method[0](f,df, interval, Num), range(Num))).T
            root=res[0][-1]
            res[0] -= res[0][-1]
            res[0] = np.abs(res[0])
            ax.plot(res[1], res[0], get_marker.next(), ms=10,
                    label="%s, (%.2f, %.2f), root=%.2f" % (method[1],interval[0],interval[1],root))
    ax.set_yscale('log')
    pl.xlabel("N, calls of f,1")
    pl.ylabel("Root Err, 1")

def f1(x):
    return x-1+12*np.exp(-2.0/x)/(1+9*np.exp(-2.0/x))
def phi(x):
    return x-(x-1+12*np.exp(-2.0/x)/(1+9*np.exp(-2.0/x)))/2.0
def df(x):
    return 1-216*np.exp(-4.0/x)/x**2/(1+9*np.exp(-2.0/x))**2+\
           24*np.exp(-2/x)/x**2/(1+9*np.exp(-2/x))

run(f1, ((-1,-0.1), (0.1, 1)), phi)

pl.grid()
pl.legend()
pl.show()

