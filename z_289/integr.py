import numpy as np
import pylab as pl
import pydb

def get_marker():
	for marker in ("*", "o", "+", "."):
		yield marker

def Simpson(f, limits, N):
	def coeff(i,N):
		if (i==0 or i==N):
			return 1
		else:
			return 2+2*(i%2)
	if (N%2 == 0):
		N -= 1
	# We belive N to be the count of the function calls
	# Simpson won't work for even N
	x,h = np.linspace(limits[0], limits[1], N, retstep = True)
	y = np.array(map(f, x))
	res = 0
	for i in range(N):
		res += coeff(i,N-1)*y[i]
	res *= h/3.0
	return res

def Rectangles(f, limits, N):
	x, h = np.linspace(limits[0], limits[1], N+1, retstep = True)
	x += h/2
	x = x[0:-1]
	return h*sum(map(f, x))

def Trapezums(f, limits, N):
	x, h = np.linspace(limits[0], limits[1], N, retstep = True)
	coeff = 2*np.ones(N)
	coeff[0] = 1
	coeff[N-1] = 1
	y = zip(x,coeff)
	F = lambda Y: Y[1]*f(Y[0])
	return h/2.0*sum(map(F,y))


def run(f1, f1_limits, f1_str, f1_val):
	"""
	Runs Simpson, Rectangles and Trapezums nitegration algorithms
	on the given function f1, with limits f1_limits. f1_string -
	fancy Tex formatted string to display in plots, f1_val - an
	accurate integral value for calculating error.
	"""
	ress1 = list()
	resr1 = list()
	rest1 = list()

	def get_err(Int, f, val, N):
		return [N, np.abs(Int-val)]
	get_err1 = lambda IntF, N: get_err(IntF, f1, f1_val,N)

	for N in map(int, np.logspace(2,13,num=10,base=2)):
		ress1.append(get_err1(Simpson(f1, f1_limits, N),N))
		resr1.append(get_err1(Rectangles(f1, f1_limits, N),N))
		rest1.append(get_err1(Trapezums(f1, f1_limits, N),N))

	ress1=np.array(ress1).T
       	resr1=np.array(resr1).T
       	rest1=np.array(rest1).T
       	f, ax = pl.subplots()
       	Data = ((ress1, "Simpson"), (rest1, "Trapezums"), (resr1, "Rectangles"))
       	markers = get_marker()
       	for data in Data:
       		ax.plot(data[0][0], data[0][1],markers.next(),
			ms=20,label="%s, %s" % (f1_str, data[1]))

       	pl.xlabel("N, calls of f,1")
       	pl.ylabel("Err, 1")
       	ax.set_yscale('log')
       	ax.legend()

run(np.exp, (0,3.0), r"$\int_0^3 \exp[x] dx$", 19.085536923187667741)
def f2(x):
	if x != 0:
		return 2*np.sqrt(np.log(2))/np.sqrt(1+(2*np.sqrt(np.log(2))*x)**3)+\
		       2/x**3/np.sqrt(np.exp(-2/x**2)+np.exp(1/x**2))
	else:
		return 2*np.sqrt(np.log(2))
run(f2, (0,1/np.sqrt(np.log(2))), r"$\int_0^\infty \frac{dx}{\sqrt{1+x^3}}$",
    2.8043642106509085224)
def expy(t):
	return np.exp(-1/t**2)
def f3(x):
	if x != 0:
		return (1+np.cos(2*expy(x)))*(2*expy(x))**(1.0/3.0)*2/x**3
	else:
		return 0
run(f3, (0, 1/np.sqrt(np.log(2))), r"$\int_0^1 \frac{1+\cos(x)}{x^{2/3}} dx$", 5.7951133217746326782)
pl.show()

